import {getConnection} from "../src/dbConn";
import {getPerfTime,createPerfTime} from "../src/queries";

describe('db connection', () => {
    test('dbconn', (done) => {
        getConnection().then(conn=>{
            done();
        })
    });
    test('getPerfTime', (done) => {
        getPerfTime().then(data=>{
            expect(Array.isArray(data)).toBe(true);
            done();
        })
    });
    test('createPerfTime', (done) => {
        createPerfTime([['test',10]]).then(()=>{
            done();
        })
    });
});
