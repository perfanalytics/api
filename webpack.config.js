const path = require('path');


module.exports ={
    mode: 'production',
    entry: './src/index.js',
    target:'node',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js'
    },
    module: {
        rules: [
            { parser: { amd: false } },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },
    optimization: {
        minimize: false
    }
};
