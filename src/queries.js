import {getConnection} from "./dbConn";

export function getPerfTime(){
    return new Promise(((resolve, reject) => {
        getConnection().then(conn=>{
            conn.query("SELECT * FROM PerfTime;" ,function (err,result) {
                if (err){
                    reject(err);
                }
                result.filter(row=>{return !/test/.test(row.name)});
                resolve(result);
            })
        });
    }))
}

export function createPerfTime(values){
    return new Promise(((resolve, reject) => {
        getConnection().then(conn=>{
            conn.query("INSERT INTO PerfTime (name, time) VALUES ?",[values] ,function (err,result) {
                if (err){
                    reject(err);
                }else {
                    resolve(result);
                }
            })
        });
    }))
}
