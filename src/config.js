export const mysqlPort = process.env["MYSQL-PORT"]
export const mysqlUser = process.env["MYSQL-USER"];
export const mysqlHost = process.env["MYSQL-HOST"];
export const mysqlPassword = process.env["MYSQL-PASSWORD"];
export const mysqlDatabase = process.env["MYSQL-DATABASE"];
export const port = process.env["PORT"];
