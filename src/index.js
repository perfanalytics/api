import express from "express";
import {port} from "./config";
import {PerfTime} from "./Route/PerfTime";
import cors from "cors";

const app = express();

app.use(express.json());
app.use(express.urlencoded());
app.options('*', cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    next();
});

app.get('/', (req, res) => {
    res.send('PerfAnalytics.Api')
});

app.use("/perfTime",PerfTime);

app.listen(port, () => {
    console.log("Server started ...")
});
