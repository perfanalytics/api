import {Router} from "express";
import {getPerfTime,createPerfTime} from "../queries";

const PerfTime = Router();



PerfTime.get("/",(req,res) => {
   getPerfTime().then(data=>{
       res.send(data);
   })
});

PerfTime.post("/",(req,res) =>{
    let perfTime=[];
    Object.keys(req.body).forEach(timeName=>{
        perfTime.push([timeName,req.body[timeName]]);
    });
    createPerfTime(perfTime).then((data)=>{
        res.sendStatus(204)
    }).catch(()=>{
        res.send({}).sendStatus(500)
    });

});

export {PerfTime}
