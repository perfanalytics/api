import mysql from "mysql"
import {mysqlPassword,mysqlPort,mysqlUser,mysqlDatabase,mysqlHost} from "./config";

let connection;
function createConnection(){
   return mysql.createConnection({
        host: mysqlHost,
        user: mysqlUser,
        password: mysqlPassword,
        port: mysqlPort,
        database: mysqlDatabase
    })
}

export function getConnection() {
    return new Promise(((resolve, reject) => {
        if (connection){
            resolve(connection);
        }else{
            createConnection().connect(function (err) {
                if (err){
                    reject(err);
                } else{
                    connection=createConnection();
                    setInterval(function () {
                        connection.query('SELECT 1');
                    }, 5000);
                    resolve(connection);
                }
            })
        }
    }))
}
